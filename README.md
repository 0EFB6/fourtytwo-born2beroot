# 📖 fourtytwo-born2beroot

Repository for 42 Cursus born2beroot project.

## 📑 Refernces
https://github.com/ayoub0x1/born2beroot.git
https://github.com/pasqualerossi/Born2BeRoot-Guide.git
https://github.com/RamonLucio/Born2beRoot.git

## 💡 About the project

> _The aim of this project is to make you code a function that returns a line, read from a file descriptor._

	You will understand how files are opened, read and closed in an OS,
	and how they are interpreted by a programming language for further analysis.
	This task is crucial to understand for a future programmer since much of the 
	time is based on manipulating files for data management and persistence.
	This project consists of coding a function that returns one line at a time from a text file.

For more detailed information, look at the [**subject of this project**](https://gitlab.com/0EFB6/fourtytwo-getnextline/-/blob/main/get_next_line.pdf).

## 🛠️ Usage

### Requirements

The function is written in C language and thus needs the **`gcc` or `cc` compiler** and some standard **C libraries** to run.

### Instructions - Using it in your code

To use the function in your code, simply include its header:

```C
#include "get_next_line.h"
```

and, when compiling your code, add the source files and the required flag:

```shell
cc -Wall -Wextra -Werror -D BUFFER_SIZE=<size> get_next_line.c get_next_line_utils.c
```

## 📋 Testing

You only have to edit the `test1.c` or `test2.c` main function and headers inside it.
You can edit test<number>.txt files to put another text if you wish to test othe cases.
Then simply run this command (change <size> and <number> with desired buffer size and file number respectively) :

```shell
gcc -Wall -Werror -Wextra -D BUFFER_SIZE=<size> get_next_line.c get_next_line_utils.c test/test<number>.c && ./a.out
```

Or you can also use this third party tester to fully test the project

* [Tripouille/gnlTester](https://github.com/Tripouille/gnlTester)

## 📝 Status

Not done.
